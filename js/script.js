//Задача №3-4
//КОД ПО ЗАДАЧЕ!
        /*function createNewUser(){
            name1 = prompt("Укажите имя:");
            sName1 = prompt("Укажите фамилию:");
            birthday = prompt("Укажите дату рождения в формате - dd.mm.yyyy");
        

            const newUser = new Object();
            this.firstName = name1;
            this.lastName = sName1;
            this.birthday1 = birthday;
            this.getLogin = () =>{
                return "Логин пользователя: " + this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();//Метод charAt() возвращает указанный символ из строки.
            }

            this.getAge = () =>{
                let now = new Date(); //Текущая дата
                let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());//Добавляем даты которые на данный момент и сбрасываем время
                let dob = new Date(this.birthday1.substr(6,10), this.birthday1.substr(3,5), this.birthday1.substr(0,2));//Добавляем дату рождения
                let age;
                //Текущий год - год рождения
                age = today.getFullYear() - dob.getFullYear();
                console.log(`Возраст пользователя: ${age}`);
            }

            this.getPassword = () =>{
                return "Обновлённый логин пользователя: " + this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday1.substr(6,10);//Метод substr() возвращает указанное количество символов из строки, начиная с указанной позиции.
            }
        }

            createNewUser();
            console.log(this.getLogin());
            console.log(this.getAge());
            console.log(this.getPassword());*/


            //ПЕРЕФОРМАТИРОВКА КОДА ПОД КЛАССЫ!
            class newUser{
                constructor(){
                    this.firstName = prompt("Укажите имя:");
                    this.lastName = prompt("Укажите фамилию:");
                    this.birthday = prompt("Укажите дату рождения в формате - dd.mm.yyyy");
                }

                getLogin(){
                return "Логин пользователя: " + this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();//Метод charAt() возвращает указанный символ из строки.
                }

                getAge(){
                let now = new Date(); //Текущая дата
                let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());//Добавляем даты которые на данный момент и сбрасываем время
                let dob = new Date(this.birthday.substr(6,10), this.birthday.substr(3,5), this.birthday.substr(0,2));//Добавляем дату рождения
                let age;
                
                //Текущий год - год рождения
                age = today.getFullYear() - dob.getFullYear();
                console.log(`Возраст пользователя: ${age}`);
                }

                getPassword(){
                return "Обновлённый логин пользователя: " + this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.substr(6,10);//Метод substr() возвращает указанное количество символов из строки, начиная с указанной позиции.
                }
            }

            class createNewUser extends newUser{
                constructor(firstName, lastName, birthday){
                    super(firstName, lastName, birthday);
                }
            }

            const funct = new createNewUser();
            console.log(funct.getLogin());
            console.log(funct.getAge());
            console.log(funct.getPassword());

           //Задача №5
            const filterBy = (arr = ["Hello my name", 19, -37, 40,"Петя", 7, "Привет", "Вася"], name) => {
                const data = arr.filter((arr) => arr.toString() === arr);
                name = data;
                console.log(name);
            }

            filterBy()

            // Виселица
            const words = [
                `программа`,
                `макака`,
                `прекрасный`,
                `оладушек`
            ];
            const word = words[Math.floor(Math.random() * words.length)];
            // Создаем итоговый массив
            const answerArray = [];
            for (let i = 0; i < word.length; i++) {
                answerArray[i] = `_`;
            }
            let remainingLetters = word.length;
            // Игровой цикл
            while (remainingLetters > 0) {
                // Показываем состояние игры
                alert(answerArray.join(" "));
                // Запрашиваем вариант ответа
                const guess = prompt(`Угадайте букву, или нажмите Отмена для выхода из игры.`);
                if (guess === null) {
                    // Выходим из игрового цикла
                    break;
                } else if (guess.length !== 1) {
                    alert(`Пожалуйста, введите одиночную букву.`);
                } else {
                // Обновляем состояние игры
                    for (let j = 0; j < word.length; j++) {
                        if (word[j] === guess) {
                        answerArray[j] = guess;
                        remainingLetters--;
                    }    
                }
            }
            // Конец игрового цикла
            }
            // Отображаем ответ и поздравляем игрока
            alert(answerArray.join(` `));
            alert(`Отлично! Было загадано слово  ${word}`);
    